# coding=utf-8
from __future__ import unicode_literals
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms.widgets import PasswordInput

__author__ = 'denis'


class LogInForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(min_length=6, widget=PasswordInput())

    def __init__(self, *args, **kwargs):
        self.user = None
        super(LogInForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        self.user = User.objects.filter(username=username).first()
        if not self.user:
            raise ValidationError('Неправильный логин!')
        return username

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if not self.user.check_password(password):
            raise ValidationError('Неверный пароль!')
        return password

    def clean(self):
        self.user = authenticate(username=self.cleaned_data.get('username'), password=self.cleaned_data.get('password'))
        return self.cleaned_data


class RegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password')

    password = forms.CharField(min_length=6, widget=PasswordInput())

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit)
        user.set_password(self.cleaned_data.get('password'))
        user.save()
        return user