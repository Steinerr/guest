# coding=utf-8
from __future__ import unicode_literals
from django.contrib import auth
from django.contrib.auth import authenticate
from django.shortcuts import redirect, render
from account.forms import LogInForm, RegisterForm

__author__ = 'denis'


def login(request):
    if request.user.is_authenticated():
        return redirect('guestbook:index')

    form = LogInForm(request.POST or None)
    if form.is_valid():
        auth.login(request, form.user)
        return redirect(request.GET.get('next', 'guestbook:index'))
    return render(request, 'account/login.html', {'form': form})


def logout(request):
    auth.logout(request)
    return redirect('guestbook:index')


def register(request):
    if request.user.is_authenticated():
        return redirect('guestbook:index')

    form = RegisterForm(request.POST or None)
    if form.is_valid():
        form.save()
        user = authenticate(username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password'))
        auth.login(request, user)
        return redirect('guestbook:index')
    return render(request, 'account/registration.html', {'form': form})