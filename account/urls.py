# coding=utf-8
from __future__ import unicode_literals
from django.conf.urls import patterns, url

__author__ = 'denis'


urlpatterns = patterns(
    'account.views',
    url(r'^login/$', 'login', name='login'),
    url(r'^logout/$', 'logout', name='logout'),
    url(r'^register/$', 'register', name='register')
)
