# coding=utf-8
from __future__ import unicode_literals
from annoying.functions import get_object_or_None
from django import forms
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView
from guestbook.models import GuestBook, Post

__author__ = 'denis'


class IndexView(ListView):
    model = GuestBook


class GuestBookCreate(CreateView):
    model = GuestBook
    fields = ('user', 'title', 'slug', 'pre_approval')

    @method_decorator(login_required(login_url='account:login'))
    def dispatch(self, *args, **kwargs):
        return super(GuestBookCreate, self).dispatch(*args, **kwargs)

    def get_initial(self):
        init = super(GuestBookCreate, self).get_initial()
        init['user'] = self.request.user.id
        return init
    
    def get_form_kwargs(self):
        form_kwargs = super(GuestBookCreate, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            fake_post = self.request.POST.copy()
            fake_post['user'] = self.request.user.id
            form_kwargs['data'] = fake_post
        return form_kwargs


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('username', 'text', 'guestbook')


class GuestBookDetail(DetailView):
    model = GuestBook

    def get_context_data(self, **kwargs):
        context = super(GuestBookDetail, self).get_context_data(**kwargs)

        data = {}
        if self.request.method == 'POST':
            data = self.request.POST.copy()
            data['guestbook'] = self.object.id

        initial = {'username': self.request.user.username if self.request.user.is_authenticated() else ''}
        form = PostForm(data or None, initial=initial)
        if form.is_valid():
            form.save()
            context['success'] = self.object.pre_approval
            form = PostForm()
        context['post_form'] = form

        posts = Post.objects.filter(guestbook=self.object)
        auth_user = self.request.user.is_authenticated()
        if not auth_user and self.object.pre_approval:
            posts = posts.filter(moderated=True)
        if self.object.pre_approval and auth_user and self.object.user_id != self.request.user.id:
            posts = posts.filter(moderated=True)
        context['posts'] = posts

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


@login_required(login_url='account:login')
def moderate_post(request, post_id):
    post = get_object_or_None(Post.objects.select_related('guestbook'), id=post_id)
    if not post:
        return redirect(request.META.get('HTTP_REFERER', 'guestbook:index'))
    if post.guestbook.user_id != request.user.id:
        return redirect(request.META.get('HTTP_REFERER', post.guestbook.get_absolute_url()))
    post.moderated = True
    post.save()
    return redirect(post.guestbook.get_absolute_url())


@login_required(login_url='account:login')
def delete_post(request, post_id):
    post = get_object_or_None(Post.objects.select_related('guestbook'), id=post_id)
    if not post:
        return redirect(request.META.get('HTTP_REFERER', 'guestbook:index'))
    if post.guestbook.user_id != request.user.id:
        return redirect(request.META.get('HTTP_REFERER', post.guestbook.get_absolute_url()))
    url = post.guestbook.get_absolute_url()
    post.delete()
    return redirect(url)