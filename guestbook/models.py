# coding=utf-8
from __future__ import unicode_literals
from django.db import models

__author__ = 'denis'


class GuestBook(models.Model):
    class Meta:
        verbose_name = 'Гостевая книга'
        verbose_name_plural = 'Гостевые книги'

    title = models.CharField(max_length=256, verbose_name='Название', unique=True)
    slug = models.SlugField(max_length=256, verbose_name='Ссылка', unique=True)
    pre_approval = models.BooleanField(default=False, verbose_name='Модерировать посты')
    user = models.ForeignKey(to='auth.User', verbose_name='Пользователь')
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)

    @models.permalink
    def get_absolute_url(self):
        return 'guestbook:guestbook', None, {'slug': self.slug}

    def __unicode__(self):
        return self.title


class Post(models.Model):
    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = ('-created', )

    username = models.CharField(max_length=128, verbose_name='Имя')
    text = models.TextField(verbose_name='Текст')
    guestbook = models.ForeignKey(to='guestbook.GuestBook', verbose_name=u'Гостевая книга')
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    moderated = models.BooleanField(default=False)

    def __unicode__(self):
        return self.username