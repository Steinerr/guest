# coding=utf-8
from __future__ import unicode_literals
from django.contrib import admin
from .models import GuestBook, Post

__author__ = 'denis'


class GuestBookAdmin(admin.ModelAdmin):
    list_display = ('user', 'slug')
    prepopulated_fields = {'slug': ('title',)}


class PostAdmin(admin.ModelAdmin):
    list_display = ('username', 'text', 'guestbook', 'moderated')
    list_filter = ('moderated', )
    search_fields = ('username', 'guestbook')
    date_hierarchy = 'created'


admin.site.register(GuestBook, GuestBookAdmin)
admin.site.register(Post, PostAdmin)