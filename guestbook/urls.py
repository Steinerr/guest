# coding=utf-8
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from .views import IndexView, GuestBookDetail, GuestBookCreate

__author__ = 'denis'


urlpatterns = patterns(
    'guestbook.views',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^add/$', GuestBookCreate.as_view(), name='guestbook_add'),
    url(r'^post/(?P<post_id>\d+)/moderate/$', 'moderate_post', name='moderate_post'),
    url(r'^post/(?P<post_id>\d+)/delete/$', 'delete_post', name='delete_post'),
    url(r'^(?P<slug>[^\.]+)/$', GuestBookDetail.as_view(), name='guestbook'),
)
